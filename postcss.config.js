module.exports = {
  plugins: [
    require("postcss-import"),
    require("postcss-nested"),
    require("postcss-custom-media"),
    require("cssnano")({
      preset: "default",
    }),
  ],
};
